 // Réglage de la scène pour les objets
 var scene = new THREE.Scene();
 var camera = new THREE.PerspectiveCamera(
 75,
 window.innerWidth / window.innerHeight,
 0.1,
 1000
 );
 var camera2 = new THREE.PerspectiveCamera(
    75,
    window.innerWidth / window.innerHeight,
    0.1,
    1000
    );
 var vector = new THREE.Vector3();
 var renderer = new THREE.WebGLRenderer();
 renderer.setSize(window.innerWidth, window.innerHeight);
 document.body.appendChild(renderer.domElement);

// Création de l'objet
 var geometry = new THREE.BoxGeometry( 1, 2, 1 );
var material = new THREE.MeshBasicMaterial( { color: "rgb(3, 197, 221)", wireframe: true, wireframeLineWidth: 1 } );
var cube = new THREE.Mesh( geometry, material );
scene.add( cube );

camera.position.z = 7;
camera.position.x = 7;


/* function animate() {
	requestAnimationFrame( animate );
	renderer.render( scene, camera );
}
animate(); */
var animate = function() {
    requestAnimationFrame(animate);
    cube.rotation.x += 0.01;
    cube.rotation.y += 0.01;
    renderer.render(scene, camera);
    };

    animate();

    // Création du second objet

    var geometry = new THREE.BoxGeometry( 1, 2, 1 );
var material = new THREE.MeshBasicMaterial( { color: "rgb(3, 197, 221)", wireframe: true, wireframeLineWidth: 1 } );
var cube2 = new THREE.Mesh( geometry, material );
scene.add( cube2 );

camera2.position.z = 7;
camera2.position.x = 4;